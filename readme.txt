This repository contains function libraries for running modified Hodgkin-Huxley neural models of cochlear implant stimulation. See model-specific readmes in each of the folders described for usage details.

Multi_Compartment: folder contains a library of functions to run segmented cable neural models stimulated by a disc electrode. This family of functions is a reimplementation and extension of the cnerve family of simulations described in, among others:
	Mino, H., Rubinstein, J.T., Miller, C.A., Abbas, P.J., 2004. Effects of Electrode-to-Fiber Distance on Temporal Neural Response with Electrical Stimulation. IEEE Trans. Biomed. Eng. 51, 13–20. doi:10.1109/TBME.2003.820383
	
	O’Brien, G.E., Imennov, N.S., Rubinstein, J.T., 2016. Simulating electrical modulation detection thresholds using a biophysical model of the auditory nerve; J. Acoust. Soc. Am. 139, 2448–2462. doi:10.1121/1.4947430
	
	Resnick, J.M., O’Brien, G., Rubinstein, J.T., 2018. Simulated auditory nerve axon demyelination alters sensitivity and response timing to extracellular stimulation. Hear. Res. 361, 121–137. doi:10.1016/j.heares.2018.01.014
	
Besides plotting scripts this code was refractored from the previous Rubinstein Lab C/MatLab code base by:	
	Jesse M Resnick © 2018
	
Single_Compartment: folder contains a library of functions for simulating a patch of mebrance stimulated by an extracellular electrode.
	

This library is based on the article and companion code described in:
	Negm, M. H., & Bruce, I. C. (2014). "The effects of HCN and KLT ion channels on adaptation and refractoriness in a stochastic auditory nerve model," IEEE Transactions on Biomedical Engineering. 61(11):2749-2759.  http://dx.doi.org/10.1109/TBME.2014.2327055.
This code base was modified to improve performance and readability by:
	Jesse M Resnick © 2018
	