% Deterministic channel test
m(1)=0.0505;
n(1)=0.3835;
h(1)=0.4782;
dt = 0.001;
generations = 10000;
V = 60;

for i =1:generations
        %voltage dependent alpha values for m,n and h
    am = (2.5-.1*V)/(exp(2.5-.1*V)-1);
    an = (1-.1*V)/(10*(exp(1-.1*V)-1));
    ah = .07*exp(-V/20);

    %voltage dependent beta values for m,n and h
    bm = 4*exp(-V/18);
    bn = 0.125*exp(-V/80);
    bh = 1/(exp(3-.1*V)+1);

    %differential equations for m,n and h
    m(i+1) = m(i)+ dt*k*(am*(1-m(i))-bm*m(i));
    n(i+1) = n(i)+ dt*k*(an*(1-n(i))-bn*n(i));
    h(i+1) = h(i)+ dt*k*(ah*(1-h(i))-bh*h(i));
end

plot(0:generations,[m;n;h]);
legend('m','n','h');

%% 
% Fast sodium channel
Naf.E_Na = 40;           % reversal potenial for sodium in mV
Naf.gNa = 20e-12;      % single channel conductance for sodium in mS
Naf.density = 618e6;

Kins.a_m_A = 6.57;  Kins.a_m_B = -27.4;  Kins.a_m_C = 10.3; 
Kins.b_m_A = 0.304; Kins.b_m_B = -25.7;  Kins.b_m_C = 9.16;

Kins.a_h_A = 0.34;  Kins.a_h_B = -114;   Kins.a_h_C = 11.0;
Kins.b_h_A = 12.6;  Kins.b_h_B = -31.8;  Kins.b_h_C = 13.4;

Naf.Kins = cell2mat(struct2cell(Kins));
clear Kins;

Kf.E_K = -84;              % " potassium "
Kf.gK = 10e-12;           % single channel conductance for Kir in mS
Kf.density = 2.0371833e7;

Kins.a_A = 0.0462; Kins.a_B = -93.2;  Kins.a_C = 1.10;  
Kins.b_A = 0.0824; Kins.b_B = -76.0;  Kins.b_C = 10.5;
Kf.Kins = cell2mat(struct2cell(Kins));
clear Kins;

% Slow potassium channel
Ks.E_K = -84;              % " potassium "
Ks.gK = 10e-12;           % single channel conductance for Kir in mS
Ks.density = 4.1160079e7;

Kins.a_A = 0.3;      Kins.a_B = -12.5;    Kins.a_C = 23.6;
Kins.b_A = 0.0036;   Kins.b_B = -80.1;    Kins.b_C = 21.8;
Ks.Kins = cell2mat(struct2cell(Kins));
clear Kins;

m(1)=0.0505;
n(1)=0.3835;
h(1)=0.4782;
s(1)= 0.5;
dt = 0.001;
generations = 10000;
V = -84;

for i =1:generations
    %voltage dependent alpha values for m,n and h
    ratesNaf = trans_rates_Naf(V,Naf.Kins);
    ratesKf = trans_rates_K(V,Kf.Kins);
    ratesKs = trans_rates_K(V,Ks.Kins);

    %differential equations for m,n and h
    m(i+1) = m(i)+ dt*k*(ratesNaf(1)*(1-m(i))-ratesNaf(2)*m(i));
    n(i+1) = n(i)+ dt*k*(ratesKf(1)*(1-n(i))-ratesKf(2)*n(i));
    h(i+1) = h(i)+ dt*k*(ratesNaf(3)*(1-h(i))-ratesNaf(4)*h(i));
    s(i+1) = s(i)+ dt*k*(ratesKs(1)*(1-s(i))-ratesKs(2)*s(i));
end

plot(0:generations,[m;n;h;s]);
legend('m','n','h','s');