function [TrialsCell] = raster_Format(SpikeNodeData,Expt)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
[dim1Length,dim2Length,dim3Length,numFibers,numMonte,numSpikes]= size(SpikeNodeData);
fiberDiameters = Expt.fibers.diameters;
[~,diameterIDX] = sort(fiberDiameters);

% Permute and reshape matrix to combine montes and fibers for raster
% plotting
totalDims = ndims(SpikeNodeData);

RefractSpikes = permute(SpikeNodeData,[1,2,3,5,4,6]);
RefractSpikes = RefractSpikes(:,:,:,:,diameterIDX,:);
Trials = reshape(RefractSpikes,dim1Length,dim2Length,dim3Length,numFibers*numMonte,1,numSpikes);

% Exclude 0 spiketimes and format into cell array.
Trials(Trials ==0) = nan;
TrialPermute = permute(Trials,[1,6,2,3,4,5]);
TrialsCell = squeeze(mat2cell(TrialPermute,ones(dim1Length,1),numSpikes,ones(dim2Length,1),ones(dim3Length,1),ones(2000,1)));

end

