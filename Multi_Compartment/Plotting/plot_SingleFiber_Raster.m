function plot_SingleFiber_Raster(spikeOutput,varargin)

p = inputParser;
addRequired(p,'spikeOutput',@(x) isnumeric(x));
addOptional(p,'monte',1,@(x) mod(x,1)==0);
addOptional(p,'Istim',inf,@(x) isvector(x));
parse(p,spikeOutput,varargin{:})

monte = p.Results.monte; Istim = p.Results.Istim;
spikeTimes = mat2cell(squeeze(spikeOutput(monte,:,:)),ones(1,size(spikeOutput,2)),size(spikeOutput,3));

if Istim == inf
    stimTime = inf;
else
    stimTime = length(Istim);
end
figure
subplot(2,1,1)
LineFormat.Color = 'k'; LineFormat.LineWidth = 1.5;
plotSpikeRaster(spikeTimes,'PlotType','vertline','XLimForCell',...
    [0,stimTime],'LineFormat',LineFormat);
ylabel('Node Number');
xlabel('Spike Time (\mus)');
subplot(2,1,2)
plot(1:stimTime,Istim,'k','LineWidth',2)
ylabel('Current mA');