function [Istim,time] = makeStim_Quadraphasic(varargin)
% Creates a stimulus current array for a 'quadraphasic' pulses as described
% in: 
% Macherey, O., Carlyon, R.P., Chatron, J., Roman, S., 2017. Effect of
%   Pulse Polarity on Thresholds and on Non-monotonic Loudness Growth in
%   Cochlear Implant Users. J. Assoc. Res. Otolaryngol. 527, 513�527.
%   doi:10.1007/s10162-016-0614-4
%
% USAGE:    [Istim,time] = makeStim_Quadraphasic
%           Istim = makeStim_Quadraphasic
%
% Optional type can be either 'anodic' or 'cathodic', referring to the
% central pulse polarity. Pulsewidth, interphase gap, interpulse interval,
% pre-pulse delay, and post-pulse wait can be passed as optional
% parameters. See parsing for default values.
%
%   Author: Jesse Resnick, 2018

p = inputParser;
validTypes = {'anodic','cathodic'};
posNonZeroInt = @(x) isempty(x) || (x >= 0 && mod(x,1) == 0);
posInt = @(x) isempty(x) || (x > 0 && mod(x,1) == 0);
addParameter(p,'type','anodic',@(x) isempty(x) || any(strcmpi(x,validTypes)))
addParameter(p,'IPG',8,posNonZeroInt) % interphase IPG (us)
addParameter(p,'pw',42,posInt) % pulsewidth (us)
addParameter(p,'delay',100,posNonZeroInt) % delay before pulse train onset(us)
addParameter(p,'wait',1000,posNonZeroInt) % wait after offset (us)
addParameter(p,'I',0.95,@(x) isempty(x) || x >= 0) % pulse current (mA)
addParameter(p,'IPI',8,posNonZeroInt) % interpulse interval (us)
parse(p,varargin{:})

type = p.Results.type;
IPG = p.Results.IPG; pw	= p.Results.pw; IPI = p.Results.IPI;
I = p.Results.I;
delay = p.Results.delay; wait = p.Results.wait;


pulseLength = 2*pw + IPG;
totDur = delay + 2*pulseLength + wait; % in us

time = 1:totDur; % us
Istim = zeros(1,totDur);

% Generate stimulus for anodic case
onsetIDX = round(delay+1);
offsetIDX = onsetIDX + round(pw);
Istim(onsetIDX:offsetIDX) = -I;
onsetIDX = offsetIDX + round(IPG);
offsetIDX = onsetIDX + round(pw);
Istim(onsetIDX:offsetIDX) = I;

onsetIDX = offsetIDX + round(IPI);
offsetIDX = onsetIDX + round(pw);
Istim(onsetIDX:offsetIDX) = I;
onsetIDX = offsetIDX + round(IPG);
offsetIDX = onsetIDX + round(pw);
Istim(onsetIDX:offsetIDX) = -I;

if strcmpi(type,'cathodic')
    Istim = -Istim;
end
end % function [Istim, time] = makeStim_Quadriphasic
