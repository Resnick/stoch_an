function [Istim,time] = makeStim_SteppedPulseUnbal(varargin)
% Creates a stimulus current array for a 'stepped' pulse
%
% USAGE:    [Istim,time] = makeStim_SteppedPulseUnbal
%           Istim = makeStim_SteppedPulseUnbal
%
% Optional type can be either 'anodic' or 'cathodic', referring to the
% polarity of the various phases. 
% Pulsewidth, interphase gap,
% pre-pulse delay, and post-pulse wait can be passed as optional
% parameters. See parsing for default values.
%
%   Author: Olivier Macherey, 2018 ; based on Jesse Resnick's makeStim_Quadraphasic.m function

p = inputParser;
validTypes = {'anodic_inc','anodic_dec','cathodic_inc','cathodic_dec'};
posNonZeroInt = @(x) isempty(x) || (x >= 0 && mod(x,1) == 0);
posInt = @(x) isempty(x) || (x > 0 && mod(x,1) == 0);
addParameter(p,'type','anodic',@(x) isempty(x) || any(strcmpi(x,validTypes)))
addParameter(p,'ISG',0,posNonZeroInt) % interstep gap ISG (us)
addParameter(p,'pw',5,posInt) % step pulsewidth (us)
addParameter(p,'nb_steps',4,posNonZeroInt) % Number of steps
addParameter(p,'delay',100,posNonZeroInt) % delay before pulse train onset(us)
addParameter(p,'wait',1000,posNonZeroInt) % wait after offset (us)
addParameter(p,'I',0.95,@(x) isempty(x) || x >= 0) % pulse current of the last step (mA)
parse(p,varargin{:})

type = p.Results.type;
ISG = p.Results.ISG; pw	= p.Results.pw; nb_steps = p.Results.nb_steps; 
I = p.Results.I;
delay = p.Results.delay; wait = p.Results.wait;


pulseLength = (pw + ISG) * nb_steps;
totDur = delay + pulseLength + wait; % in us

time = 1:totDur; % us
Istim = zeros(1,totDur);

if strcmpi(type,'anodic_inc') || strcmpi(type,'cathodic_inc'),
    
    % Generate stimulus for anodic increasing case
    onsetIDX = round(delay+1);
    for j=1:nb_steps,
       
        offsetIDX = onsetIDX + round(pw);
        Istim(onsetIDX:offsetIDX) = I*(j/nb_steps);
        onsetIDX = offsetIDX + round(ISG);
        
    end
        

    if strcmpi(type,'cathodic_inc')
        Istim = -Istim;
    end

elseif strcmpi(type,'anodic_dec') || strcmpi(type,'cathodic_dec')
    
       % Generate stimulus for anodic increasing case
    onsetIDX = round(delay+1);
    for j=1:nb_steps,
       
        offsetIDX = onsetIDX + round(pw);
        Istim(onsetIDX:offsetIDX) = I*((nb_steps-j+1)/nb_steps);
        onsetIDX = offsetIDX + round(ISG);
        
    end

    if strcmpi(type,'cathodic_dec')
        Istim = -Istim;
    end
end

end % function [Istim, time] = makeStim_SteppedPulseUnbal
