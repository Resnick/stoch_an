function [Istim,time] = makeStim_SinglePulse(varargin)
% Creates a stimulus list for either monophasic or biphasic square pulses.
% Biphasic pulses are like those used in Shepherd and Javel Correlating
% Cochlear Status paper. He used pulse trains, we use a single impulse.
%
% USAGE:    [Istim,time] = makeStim_SinglePulse
%           Istim = makeStim_SinglePulse
%
% See parsing for optional arguements and default/compatible values.
%
%   Author: Jesse Resnick, 2018

p = inputParser;
validTypes = {'anodic','cathodic','AF','CF'};
addParameter(p,'type','af',@(x) isempty(x) || any(strcmpi(x,validTypes)))
addParameter(p,'IPG',8,@(x) isempty(x) || (x >= 0 && mod(x,1) == 0)) % interphase IPG in us
addParameter(p,'pw',25,@(x) isempty(x) || (x > 0 && mod(x,1) == 0)) % pulsewidth in us
addParameter(p,'delay',100,@(x) isempty(x) || (x >= 0 && mod(x,1) == 0)) % delay before pulse train onset in us
addParameter(p,'wait',1000,@(x) isempty(x) || (x >= 0 && mod(x,1) == 0)) % wait after offset in us
addParameter(p,'I',2,@(x) isempty(x) || x >= 0) % main pulse current (mA)
parse(p,varargin{:})

type = p.Results.type;
IPG = p.Results.IPG; pw	= p.Results.pw;
I = p.Results.I;
delay = p.Results.delay; wait = p.Results.wait;

switch lower(type)
    case {'cf','af'}
        pulseLength = 2*pw + IPG;
    case {'anodic','cathodic'}
        pulseLength = pw;
end
totDur = delay + pulseLength + wait; % in us
time = 1:totDur; % us
Istim = zeros(1,totDur);

if IPG
    switch lower(type)
        case 'cf'
            onsetIDX = round(delay+1);
            offsetIDX = onsetIDX + round(pw);
            Istim(onsetIDX:offsetIDX) = -I;
            onsetIDX = offsetIDX + round(IPG);
            offsetIDX = onsetIDX + round(pw);
            Istim(onsetIDX:offsetIDX) = I;
            
        case 'af'
            onsetIDX = round(delay+1);
            offsetIDX = onsetIDX + round(pw);
            Istim(onsetIDX:offsetIDX) = I;
            onsetIDX = offsetIDX + round(IPG);
            offsetIDX = onsetIDX + round(pw);
            Istim(onsetIDX:offsetIDX) = -I;
            
        case 'cathodic'
            warning('No IPG for monophasic pulses.')
            onsetIDX = round(delay+1);
            offsetIDX = onsetIDX + round(pw);
            Istim(onsetIDX:offsetIDX) = -I;
            
        case 'anodic'
            warning('No IPG for monophasic pulses.')
            onsetIDX = round(delay+1);
            offsetIDX = onsetIDX + round(pw);
            Istim(onsetIDX:offsetIDX) = I;
            
    end
else
    switch lower(type)
        case 'cf'
            onsetIDX = round(delay+1);
            offsetIDX = onsetIDX + round(pw);
            Istim(onsetIDX:offsetIDX) = -I;
            onsetIDX = offsetIDX;
            offsetIDX = onsetIDX + round(pw);
            Istim(onsetIDX:offsetIDX) = I;
            
        case 'af'
            onsetIDX = round(delay+1);
            offsetIDX = onsetIDX + round(pw);
            Istim(onsetIDX:offsetIDX) = I;
            onsetIDX = offsetIDX;
            offsetIDX = onsetIDX + round(pw);
            Istim(onsetIDX:offsetIDX) = -I;
            
        case 'cathodic'
            onsetIDX = round(delay+1);
            offsetIDX = onsetIDX + round(pw);
            Istim(onsetIDX:offsetIDX) = -I;
            
        case 'anodic'
            onsetIDX = round(delay+1);
            offsetIDX = onsetIDX + round(pw);
            Istim(onsetIDX:offsetIDX) = I;
            
    end
end

end % function [time, stim] = makeStim_SinglePulse
