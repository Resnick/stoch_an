%% Population set-up
% Set Population Moment Parameters based on data
% 2 mm Imennov & Rubinstein 2009. 1.477 mm Wan & Corfas 2017
diamMean	= 1.477;	

% 0.5 mm Imennov & Rubinstein 2009. 0.22 mm matched to Wan & Corfas 2017
diamStdev	= 0.22;     

% dtoD of healthy neurons. 0.643 Wan & Corfas 2017
normdtoDmu    = 0.643;   
normdtoDsigma    = 0.0738;

anamProps(1) = diamMean;
anamProps(2) = diamStdev;
anamProps(3) = normdtoDmu;
anamProps(4) = normdtoDsigma;
    
numFibers = 2; % changed this from 200 to 100
%% Set fiber-independent model properties
model   = model_Config;

%% Initialize fiber population
Fibers = initialize_Population(numFibers,anamProps,model);

%% Set experimental options
options = options_Config;
options.numMonte = 1;
options.recV = 1;
options.recECAP = 1;

%% Generate SinglePulse stimulus
% stimOptions.I = 5;          % in uA
% % % stimOptions.testI = 1.15;
% % % stimOptions.IPI = 2000;
% stimOptions.wait = 1200;                 % simulation time in us
% stimOptions.delay = 300;           % pulse onset time in us
% stimOptions.pw = 500;                   % pulse width in us
% stimOptions.IPG = 1;                  % interphase gap in us
% stimOptions.type = 'af';
% 
% stimOptsCell = struct2varargin(stimOptions);
% Istim = makeStim_SinglePulse(stimOptsCell{:});

%% Generate PrePulseBal Stimulus


% stimOptions.I_pre = 0.076; 
% stimOptions.pw_pre = 500;               
% 
% 
% stimOptions.I_test = 0.08;  
% stimOptions.pw_test = 25;               
% 
% 
% stimOptions.wait = 1200;           % simulation time in us
% stimOptions.delay = 300;           % pulse onset time in us
% stimOptions.IPG = 1;               % interphase gap in us
% stimOptions.type = 'AnodCathod';

% Calling the stimulating current functions
% stimOptsCell = struct2varargin(stimOptions);
Istim = makeStim_PrePulseBal();


%% Run the simulations
[spikeOutput,vOutput,~] = Population_Run(Fibers,Istim,model,options);

%% Plotting vOutput
% 
% for fig = 1:numFibers
%     figure
%     temp = vOutput{fig};
%     imagesc(squeeze(temp(:,:,:)-84),[-85,50]);
%     c = colorbar; c.Label.String = 'Voltage (mV)';
%     xlabel('Segment #');
%     ylabel('Timestep (x 10^-^1 \mus)');
%     if fig == 1
%         title('diameter = 1.5 \mum')
%     else
%         title('diameter = 3.0 \mum')
%     end
%     
% end

%% Plot it out

recSpikes = nan(numFibers,options.numMonte,options.maxSpikes);
for fibIDX = 1:numFibers
    nodeInt = round(Fibers.numNodes(fibIDX)*options.posRecord);
    recSpikes(fibIDX,:,:) = squeeze(spikeOutput{fibIDX}(:,nodeInt,:));
end

simtime = length(Istim)-1;
recSpikes = permute(recSpikes,[2,1,3]);
spikeTimes = reshape(recSpikes,[numFibers*options.numMonte,options.maxSpikes]);
spikeTimes = mat2cell(spikeTimes,ones(1,numFibers*options.numMonte),options.maxSpikes);

% figure
% subplot(2,1,1)
% LineFormat.Color = 'k'; LineFormat.LineWidth = 1.5;
% plotSpikeRaster(spikeTimes,'PlotType','vertline','XLimForCell',...
%     [0,2500],'LineFormat',LineFormat);
% ylabel('Trial Number');
% xlabel('Spike Time (\mus)');
% subplot(2,1,2)
% plot(0:simtime,Istim,'k','LineWidth',2)
% xlabel('Spike Time (\mus)');
% ylabel('Current (mA)')

%% Plot out eCAP

% figure(2); plot(0:simtime,eCAPout)

%% getting latencies

latencies = recSpikes(:,:,1) - 300;
numSpikesSmall = numSpikesSmall - sum(isnan(latencies(1)));
numSpikesLarge = numSpikesLarge - sum(isnan(latencies(2)));
% meanlatency = nanmean(latencies);
