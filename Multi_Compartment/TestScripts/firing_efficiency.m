%% function use for Hyak
function firing_efficiency(ExptName, dataDir, codeDir)

% tic;

numberFibers = 1000;
numSpikesSmall = numberFibers;
numSpikesLarge = numberFibers;
%% Monte Carlos
for MonteCarlo = 1:numberFibers
    Pop_Test
end

firingEffSmall = numSpikesSmall / numberFibers;
firingEffLarge = numSpikesLarge / numberFibers;

%% Save overall experiment info
cd(dataDir);
if ~isfolder(ExptName)
    mkdir(ExptName);
end
cd(ExptName); ExptFolder = pwd;
ExptFile = sprintf('%s%sExpt.mat',ExptFolder,filesep);

if ~ispc
    save(ExptFile,'firingEffSmall', 'firingEffLarge');
else
    save(ExptFile,'firingEffSmall', 'firingEffLarge');
end

end



%% Calculating Firing efficiency based on the PrePulse 
% start b

% % tic;
% 
% numberFibers = 250;
% numSpikesSmall = numberFibers;
% numSpikesLarge = numberFibers;
% %% Monte Carlos
% for MonteCarlo = 1:numberFibers
%     Pop_Test
% end
% 
% % firingEffSmall = 0.68;
% 
% firingEffSmall = numSpikesSmall / numberFibers;
% firingEffLarge = numSpikesLarge / numberFibers;


%% Graphing
% 
% thresholds = [firingEffSmall; firingEffLarge];
% x = [1.5; 3.0];
% 
% str = ['pre-pulse width = 50 \musec , 30% of large fiber threshold ; small firing efficiency =' num2str(firingEffSmall)];
% % str = ['Pulse-Width = 50 \musec, ' 'small firing efficiency =' num2str(firingEffSmall)];
% % str = ['small firing efficiency = ' num2str(firingEffSmall) ' large firing efficiency = ' num2str(firingEffLarge)];
% 
% 
% figure
% subplot(2,1,1)
% 
% bar(x,thresholds)
% ylim([0 1.1]) 
% title(str)
% ylabel('Firing Efficiency')
% xlabel('fiber diameter (\mum)')
% 
% subplot(2,1,2)
% plot(0:simtime,Istim,'k','LineWidth',2)
% xlabel('Spike Time (\mus)');
% ylabel('Current (mA)')
% 
% 
% load train
% sound(y,Fs)
% 
% toc;
