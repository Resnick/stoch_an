function [spikeOutput,vOutput,eCAPout] = Stoch_Test(I0)
model = model_Config;

%% Set fiber properties

diamMean = 1.5;
fiber.diameter = 3.0;      % axolemma diameter (um)
                    % changing this value to get different diameter fibers

normdtoDmu      = 0.643;
normdtoDsigma   = 0.0738;
fiber.dtoD = normdtoDmu+2*normdtoDsigma;             % d/D
% fiber.cutoff = [];            % PLACEHOLDER: demyelination extent
% fiber.demydtoD = [];          % PLACEHOLDER: demyelination severity

fiber.numNodes = ceil(diamMean/fiber.diameter*36);
fiber.zDistance = 3;            % distance of fiber from electrode center (mm)
fiber.intSegs = ceil(model.intSegs*fiber.diameter/diamMean);



%% Set experimental options
options = options_Config;
options.numMonte = 1;
options.recV = 1;
options.recECAP = 0;
options.maxSpikes = 10;
options.XstimLoc = -(model.LtoD*diamMean*1e-3/...
    model.normdtoD+model.nLen)*options.meanNodeAbove;

%% Generate stimulus

% making SinglePulse stimulus

stimOptions.I = I0; 
stimOptions.type = 'af';
stimOptions.delay = 300; % in us
stimOptions.pw = 500; % in us
stimOptions.IPG = 1; % in us
stimOptions.wait = 1200; % in us


% stimOptions.condI = 0;
% stimOptions.testI = I0;

% stimOptions.I_pre = 0.2; % for PrePulseBal
% stimOptions.I_test = 1;  % for PrePulseBal

% stimOptions.IPI   = 1000;
% stimOptions.wait = 2000;           % simulation time in us
% stimOptions.delay = 2000;           % pulse onset time in us
% stimOptions.pw = 25;               % pulse width in us
% stimOptions.IPG = 8;               % interphase gap in us
% stimOptions.type = 'cf';

% Calling the stimulating current functions
stimOptsCell = struct2varargin(stimOptions);
Istim = makeStim_SinglePulse(stimOptsCell{:});
% Istim = makeStim_SinglePulse();
% Istim = makeStim_PairedPulse(stimOptsCell{:});
% Istim = makeStim_PrePulseBal(stimOptsCell{:});
% makeStim_PrePulseBal
            % switch makeStim for another one in the folder 
            % PrePulseBal

% stimOptions.wait = 1500;                 % simulation time in us
% stimOptions.delay = 500;           % pulse onset time in us
% stimOptions.pw = 40;                   % pulse width in us
% stimOptions.IPG = 8;                  % interphase gap in us
% stimOptions.type = 'cf';
% stimOptions.I = I0;
% stimOptsCell = struct2varargin(stimOptions);
% Istim = makeStim_SinglePulse(stimOptsCell{:});

%% Calculate segment properties from fiber and model info
segments = fiber_initialization(fiber,model);

%% Run model
% Need consistent field order for MEX function.
segments = orderfields(segments);
model = orderfields(model);
options = orderfields(options);
tic;
[spikeOutput,vOutput,eCAPout] = stochAN_multi_mex(Istim,segments,model,options);
toc;

%%
simtime = length(Istim)-1;
if options.recV
    figure
    imagesc(squeeze(vOutput(1,:,:)-84),[-85,50]);
    c = colorbar; c.Label.String = 'Voltage (mV)';
    xlabel('Segment #');
    ylabel('Timestep (\mus)');
%     
%     
%     figure
%     subplot(2,1,1)
%     simtime = length(Istim)-1;
%     for i = 1:(model.intSegs+1):size(vOutput,3)
%         plot(0:options.Vsample/model.dt:simtime,vOutput(1,:,i)-84); hold on;
%     end
%     subplot(2,1,2)
%     plot(0:model.dt:simtime/1000,Istim,'k','LineWidth',2)
%     xlabel('Timestep (\mus)');
%     ylabel('Current (mA)');
end
% 
% if options.recECAP
%     figure
%     plot(0:1:simtime,eCAPout);
% end
% 
% spikeTimes = mat2cell(squeeze(spikeOutput(:,:,:)),ones(1,size(spikeOutput,2)),size(spikeOutput,3));
% 
% figure
% subplot(2,1,1)
% LineFormat.Color = 'k'; LineFormat.LineWidth = 1.5;
% plotSpikeRaster(spikeTimes,'PlotType','vertline','XLimForCell',...
%     [0,simtime],'LineFormat',LineFormat);
% ylabel('Node Number');
% xlabel('Spike Time (\mus)');
% subplot(2,1,2)
% plot(0:model.dt:simtime/1000,Istim,'k','LineWidth',2)
% xlabel('Timestep (ms)');
% ylabel('Current (mA)');

end