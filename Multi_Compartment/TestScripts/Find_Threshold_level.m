%% Produces a range of current spanning the dynamic range of each fiber for which the model does not crash

clear all;
close all;

%% Population set-up
anamprops.diameters=[0.75 1.5 3 6];
anamprops.zs= [6 3 1];
normdtoDmu      = 0.643;
normdtoDsigma   = 0.0738;
anamprops.dtoDs=[normdtoDmu-2*normdtoDsigma  normdtoDmu normdtoDmu+2*normdtoDsigma];

[zs, diameters, dtoDs] = handpick_Population(anamprops);

%% Generate stimulus
stimOptions.wait = 3000;                 % simulation time in us
stimOptions.delay = 500;           % pulse onset time in us
stimOptions.pw = 42;                   % pulse width in us
stimOptions.IPG = 8;                  % interphase gap in us
stimOptions.IPI = 8;
stimOptions.type = 'cathodic';

%% Initialize Min and Max levels
LevelMin(1:length(diameters))=NaN;
LevelMax(1:length(diameters))=NaN;

%% Start procedure for each fiber

for j=length(diameters):-1:1, % Start with larger diameter and smaller distance because lowest threshold of all conditions (if it works there, it works everywhere)

    j
    current=0.05; % initial current in uA
    dBStep=1; % increment between current values
    DynamicRange=100; % Max
    NbFirings=0;
    bThresholdReached=0; % boolean whose value changes when spike is produced
    Vmax=1000;  % To be conservative as the simulation crashes when V > 1500
    SpikesMax=20; % To be conservative as the simulation crashes when number of spieks exceeds 25

    for i=1:DynamicRange,
    
        stimOptions.I = current;
        stimOptsCell = struct2varargin(stimOptions);
        Istim = makeStim_Quadraphasic(stimOptsCell{:}); % Make the stimulus at current level I
    
        [spikeOutput,vOutput] = Stoch_Test_Olivier(Istim,diameters(j),dtoDs(j),zs(j));
        NbFirings_vec(i)=length(nonzeros(isfinite((spikeOutput(:,:,2)))));
    
        if max(max(abs(vOutput(1,:,:)))) > Vmax || NbFirings_vec(i) > SpikesMax, 
            LevelMax(j)=current;
            break; % Stop incrementing when vOutput > max voltage or when number of spikes > max spikes to avoid crash of stochastic model
        end
    
        if NbFirings_vec(i) ~= 0 && NbFirings ~= 0 && bThresholdReached==0,
            LevelMin(j)=current/10^(dBStep/20);
            bThresholdReached=1;
        end
    
        current=current*10^(dBStep/20);
        NbFirings=NbFirings_vec(i)
    end

    LevelMin % Minimum level at which there is reliable spiking (need to use something smaller for the real simulation)
    LevelMax % HIgh level at which the model does not crash
    
end
