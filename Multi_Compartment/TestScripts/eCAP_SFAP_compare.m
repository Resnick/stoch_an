function eCAP = eCAP_SFAP_compare(I0)
%% Population set-up
% Set Population Moment Parameters based on data
% 2 mm Imennov & Rubinstein 2009. 1.477 mm Wan & Corfas 2017
diamMean	= 1.477;	

% 0.5 mm Imennov & Rubinstein 2009. 0.22 mm matched to Wan & Corfas 2017
diamStdev	= 0.5;     

% dtoD of healthy neurons. 0.643 Wan & Corfas 2017
normdtoDmu    = 0.643;   
normdtoDsigma    = 0.0738;

anamProps(1) = diamMean;
anamProps(2) = diamStdev;
anamProps(3) = normdtoDmu;
anamProps(4) = normdtoDsigma;
    
numFibers = 20;
%% Set fiber-independent model properties
model   = model_Config;
model.Terminal.areaCoef = 20;
%% Initialize fiber population
Fibers = initialize_Population(numFibers,anamProps,model);

%% Set experimental options
options = options_Config;
options.numMonte = 2;
options.recV = 0;
options.recECAP = 1;

%% Generate stimulus
stimOptions.I = 0.5;
stimOptions.wait = 2000;           % simulation time in us
stimOptions.delay = 2000;           % pulse onset time in us
stimOptions.pw = 25;               % pulse width in us
stimOptions.IPG = 8;               % interphase gap in us
stimOptions.type = 'af';

% Calling the stimulating current functions
stimOptsCell = struct2varargin(stimOptions);
Istim_masked = makeStim_SinglePulse(stimOptsCell{:});
maskedI = stimOptions.I;

stimOptions.I = I0;
stimOptions.type = 'af';

% Calling the stimulating current functions
stimOptsCell = struct2varargin(stimOptions);
Istim_unmasked = makeStim_SinglePulse(stimOptsCell{:});

%% Run the simulations
[~,~,SFAPsMasked] = Population_Run_SFAP(Fibers,Istim_masked,model,options);
[~,~,SFAPsOut] = Population_Run_SFAP(Fibers,Istim_unmasked,model,options);

%% Plot out eCAP
eCAPMask = reshape(sum(SFAPsMasked,1),options.numMonte,length(SFAPsMasked));
eCAPOut = reshape(sum(SFAPsOut,1),options.numMonte,length(SFAPsOut));
for monteIDX = 1:options.numMonte
    eCAPMask(monteIDX,:) = (eCAPMask(monteIDX,:)-mean(eCAPMask(monteIDX,:)))*(I0/maskedI)+mean(eCAPMask(monteIDX,:));
end
eCAP = eCAPOut-eCAPMask;

figure
for fibIDX = 1:numFibers-2
    plot(squeeze(mean(SFAPsOut(fibIDX,:,1000:end),2)),'Color',[0.5,0.5,0.5],'LineWidth',1); hold on
end
ylabel('SFAP (\muV)','Color',[0.3,0.3,0.3]); ylim([-1.2e-4,2.5e-5])
yyaxis right
plot(mean(eCAPOut(:,1000:end),1)*1000,'k','LineWidth',2);
set(gca,'FontSize',14)
xlim([750,2500]); xlabel('Time (\mus)');
ylabel('eCAP (\muV)','Color','k'); ylim([-1.2,.25])
