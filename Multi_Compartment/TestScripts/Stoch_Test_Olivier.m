function [spikeOutput,vOutput] = Stoch_Test_Olivier(Istim,diam_val,dtoD_val,zDistance_val)
model = model_Config;

%% Set fiber properties
diamMean = 1.5;
fiber.diameter = diam_val;      % axolemma diameter (um)
fiber.dtoD = dtoD_val;             % d/D
normdtoDmu      = 0.643;
normdtoDsigma   = 0.0738;
% fiber.cutoff = [];            % PLACEHOLDER: demyelination extent
% fiber.demydtoD = [];          % PLACEHOLDER: demyelination severity

fiber.numNodes = ceil(diamMean/fiber.diameter*36);
fiber.zDistance = zDistance_val;            % distance of fiber from electrode center (mm)
fiber.intSegs = ceil(model.intSegs*fiber.diameter/diamMean);


%% Set experimental options
options = options_Config;
options.numMonte = 1;
options.recV = 1;
options.XstimLoc = -(model.LtoD*diamMean*1e-3/...
    model.normdtoD+model.nLen)*options.meanNodeAbove;

%% Calculate segment properties from fiber and model info
segments = fiber_initialization(fiber,model);

%% Run model
% Need consistent field order for MEX function.
segments = orderfields(segments);
model = orderfields(model);
options = orderfields(options);
tic;
if options.recV
    [spikeOutput,vOutput] = stochAN_multi_mex(Istim,segments,model,options);
else
    spikeOutput = stochAN_multi_mex(Istim,segments,model,options);
end
toc;

%%
% if options.recV
%     figure
%     imagesc(squeeze(vOutput(1,:,:)-84),[-inf,inf]);
%     c = colorbar; c.Label.String = 'Voltage (mV)';
%     xlabel('Segment #');
%     ylabel('Timestep (\mus)');
%     
%     
%     figure
%     subplot(2,1,1)
%     simtime = length(Istim);
%     for i = 1:(model.intSegs+1):size(vOutput,3)
%        plot(0:options.Vsample/model.dt:simtime-1,vOutput(1,:,i)-84); hold on;
%     end
%     subplot(2,1,2)
%     plot(0:model.dt:simtime/1000-model.dt,Istim,'k','LineWidth',2)
% end
% 
% spikeTimes = mat2cell(squeeze(spikeOutput(:,:,1)).',ones(1,size(spikeOutput,2)),options.numMonte);
% 
% figure
% subplot(2,1,1)
% LineFormat.Color = 'k'; LineFormat.LineWidth = 1.5;
% plotSpikeRaster(spikeTimes,'PlotType','vertline','XLimForCell',...
%     [0,simtime],'LineFormat',LineFormat);
% ylabel('Node Number');
% xlabel('Spike Time (\mus)');
% subplot(2,1,2)
% plot(0:model.dt:simtime/1000-model.dt,Istim,'k','LineWidth',2)
% 
% end