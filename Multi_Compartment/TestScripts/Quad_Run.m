function [spikeOutput,vOutput] = Quad_Run(I,type)
%% Set model properties
model = model_Config; % Function with model parameters in it.

%% Set fiber properties
diamMean = 1.477;
fiber.diameter = diamMean;      % axolemma diameter (um)
fiber.dtoD = 0.643;             % d/D
% fiber.cutoff = [];            % PLACEHOLDER: demyelination extent
% fiber.demydtoD = [];          % PLACEHOLDER: demyelination severity

fiber.numNodes = 36;
fiber.zDistance = 3;            % distance of fiber from electrode center (mm)
fiber.intSegs = model.intSegs*fiber.diameter/diamMean;

%% Set experimental options
options = options_Config;
options.numMonte = 2;
options.recV = 1;
options.XstimLoc = -(model.LtoD*diamMean*1e-3/...
    model.normdtoD+model.nLen)*options.meanNodeAbove;

%% Generate stimulus
stimOptions = {'I',I,'type',type};

Istim = makeStim_Quadraphasic(stimOptions{:});
simtime = length(Istim);

%% Calculate segment properties from fiber and model info 
segments = fiber_initialization(fiber,model);

%% Run model
    % Need consistent field order for MEX function.
    segments = orderfields(segments);
    model = orderfields(model);
    options = orderfields(options);
tic;
if options.recV
    [spikeOutput,vOutput] = stochAN_multi_mex(Istim,segments,model,options);
else
    spikeOutput = stochAN_multi_mex(Istim,segments,model,options);
end
toc;

%% Plot voltage data
if options.recV
    dt = model.dt;
    numSegments = length(segments.cm);
    figure
    imagesc([1,numSegments],[0,simtime],squeeze(vOutput(1,:,:)-84));
    c = colorbar; c.Label.String = 'Voltage (mV)';
    xlabel('Fiber Segment #');
    ylabel('Timestep (\mus)');
    
    figure
    subplot(2,1,1)
    legEntry = cell(0);
    for i = 11:60:numSegments
        plot(0:options.Vsample/dt:simtime-1,vOutput(1,:,i)-84); hold on;
        legEntry = [legEntry, {num2str((i-1)/10+1)}];
    end
    legHand = legend(legEntry);
    title(legHand,'Node #')
    xlabel('Timestep (\mus)');
    ylabel('Membrane Potential (mV)');
    subplot(2,1,2)
    plot(0:1:simtime-1,Istim,'k','LineWidth',2)
end

%% Plot spiking data
recSpikes = squeeze(spikeOutput(:,32,:));
spikeTimes = mat2cell(recSpikes,ones(1,options.numMonte),options.maxSpikes);

figure
subplot(2,1,1)
LineFormat.Color = 'k'; LineFormat.LineWidth = 1.5;
plotSpikeRaster(spikeTimes,'PlotType','vertline','XLimForCell',...
    [0,simtime*1000],'LineFormat',LineFormat);
ylabel('Trial Number');
xlabel('Timestep (\mus)'); xlim([0,simtime]);
subplot(2,1,2)
plot(0:1:simtime-1,Istim,'k','LineWidth',2)
end