figure('position',[50,50,1200,800])
t = 2200; % Sequence 1950,2200,2300,2500
% Pulses 2010/2050

imagesc(-eCAPxs,eCAPzs,squeeze(eCAPout(:,:,1,t))*1000); hold on; caxis([-.04,.04])
plot(segments.xApp,zeros(length(segments.xApp),1),'k','LineWidth',8); hold on;
plot(-options.eCAPxLoc,options.eCAPzLoc,'.k','MarkerSize',35);
Cbar = colorbar('northoutside');
Cbar.Label.String = 'Potential (mV)';
xlim([-1,8]); ylim([0,5]);
axis tight manual
% xticklabels([]);
xlabel ('Distance re: Periphery (mm)')
% ylabel('Radial Distance (mm)');
set(gca,'FontSize',24);
ax = gca;
ax.YDir = 'normal';
box off