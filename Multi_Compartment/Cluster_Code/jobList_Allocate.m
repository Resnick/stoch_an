function [jobList,nodesRequested] = jobList_Allocate(numFibers,numMontes,allStim)
%JOBLIST_ALLOCATE Allocates stimuli and, eventually, fibers to different
%trials to optimize job length.

% Maximum desired job duration (s)
corePerProc = 14;
procPerNode = 2;
realToJobTime = 3300; % simulation duration that runs in ~1s of core time
targetJobDuration = 30; % (minutes)
maxNodeTime = targetJobDuration*corePerProc*procPerNode*60*realToJobTime; % us
maxJobTime = maxNodeTime/procPerNode; % Target job time

% Determine durations of all trials
stimLength  = zeros(1,numel(allStim));
for stimIDX = 1:numel(allStim)
    stimLength(stimIDX) = length(allStim{stimIDX});
end
stimDurs = numFibers*numMontes*stimLength;
totalDur = sum(stimDurs); % Total simulation duration (us)

nodesNeeded = ceil(totalDur/maxNodeTime);

% Determine approximate length of simulation runs
jobList = {}; jobDur = 0; listIDX = 1;
if any(stimDurs > maxJobTime)
    error('At least one one stimulus/population pairing leads to simulations that are too long.');
end

for stimIDX = 1:numel(allStim)
    jobDur = jobDur + stimDurs(stimIDX);
    if jobDur < maxJobTime
        if stimIDX == 1
            jobList{listIDX} = stimIDX; %#ok<*AGROW>
        else
            jobList{listIDX} = [jobList{listIDX}, stimIDX];
        end
    else
        listIDX = listIDX + 1;
        jobDur = stimDurs(stimIDX);
        jobList{listIDX} = stimIDX;
    end
end

% Request more than strictly necessary to accelerate.
nodesRequested = min(nodesNeeded*5,length(jobList));
