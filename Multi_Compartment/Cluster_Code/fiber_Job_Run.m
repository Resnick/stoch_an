function [spikeOutput,vOutput] = fiber_Job_Run(ExptFile,popIDX,stimIDXs,SaveFile)
%JOB_RUN Runs a fiber population through allStims{stimIDXs,:} of stimulus
%waveforms where each fiber recieves stimuli of tuned intensity.

Expt = load(ExptFile,'fibPop','allStims','model','options');
dataDir = pwd;
Fibers = Expt.fibPop{popIDX};
spikeOutput = cell(length(stimIDXs),1);

if Expt.options.recV; vOutput = cell(length(stimIDXs),1); end

for trialIDX = 1:length(stimIDXs)
    stimIDX =stimIDXs(trialIDX);
    stimList = Expt.allStims(stimIDX,:);
        if Expt.options.recV
            [spikeOutput{trialIDX},vOutput{trialIDX}] = Fibers_Run(Fibers,stimList,Expt.model,Expt.options);
        else
            spikeOutput{trialIDX} = Fibers_Run(Fibers,stimList,Expt.model,Expt.options);
        end
end

cd(dataDir); %Make sure we're back in the correct directory
if Expt.options.recV
    save(SaveFile,'spikeOutput','vOutput','stimIDXs');
else
    save(SaveFile,'spikeOutput','stimIDXs');
end

