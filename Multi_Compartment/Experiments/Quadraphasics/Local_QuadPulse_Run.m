% Local Quad Run

%% Population set-up
% Set Population Moment Parameters based on data
% 2 mm Imennov & Rubinstein 2009. 1.477 mm Wan & Corfas 2017
diamMean	= 1.477;

% 0.5 mm Imennov & Rubinstein 2009. 0.22 mm matched to Wan & Corfas 2017
diamStdev	= 0.22;

% dtoD of healthy neurons. 0.643 Wan & Corfas 2017
normdtoDmu      = 0.643;
normdtoDsigma   = 0.0738;

numFibers = 3;
%% Set fiber-independent model properties
model   = model_Config;

%% Initialize fiber populations
anamProps(1) = diamMean;
anamProps(2) = diamStdev;
anamProps(3) = normdtoDmu;
anamProps(4) = normdtoDsigma;

Fibers = initialize_Population(numFibers,anamProps,model);

%% Set experimental options
options = options_Config;
options.numMonte = 5;
options.recV = 0;

%% Generate stimuli
I = 0.5:0.1:7;
stimOptions.wait = 3000;                 % simulation time in us
stimOptions.delay = 500;           % pulse onset time in us
stimOptions.pw = 42;                   % pulse width in us
stimOptions.IPG = 8;                  % interphase gap in us
stimOptions.IPI = 8;
stimOptions.type = 'cathodic';
allStims = cell(length(I),1);

for curIDX = 1: length(I)
    stimOptions.I = I(curIDX);
    stimOptsCell = struct2varargin(stimOptions);
    allStims{curIDX} = makeStim_Quadraphasic(stimOptsCell{:});
end

% Save all the stimulus values after creating allStims.
stimOptions.I = I;
%% Run expt
if options.recV
    [allSpikeOutput,allVoltOut] = local_Expt_Run(Fibers,allStims,model,options);
else
    allSpikeOutput = local_Expt_Run(Fibers,allStims,model,options);
end