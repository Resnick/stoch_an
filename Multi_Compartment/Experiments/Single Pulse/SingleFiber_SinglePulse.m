function [spikeOutput,vOutput] = SingleFiber_SinglePulse(I0,dataDir,exptName)
model = model_Config;

%% Set fiber properties
diamMean = 1.47;
fiber.diameter = diamMean;      % axolemma diameter (um)
fiber.dtoD = 0.643;             % d/D
% fiber.cutoff = [];            % PLACEHOLDER: demyelination extent
% fiber.demydtoD = [];          % PLACEHOLDER: demyelination severity

fiber.numNodes = 36;
fiber.zDistance = 3;            % distance of fiber from electrode center (mm)
fiber.intSegs = ceil(model.intSegs*fiber.diameter/diamMean);

%% Set experimental options
options = options_Config;
options.numMonte = 1;
options.recV = 1;
options.XstimLoc = -(model.LtoD*diamMean*1e-3/...
    model.normdtoD+model.nLen)*options.meanNodeAbove;

%% Generate stimuli
I = I0;
stimOptions.wait = 2000;           % simulation time in us
stimOptions.delay = 500;           % pulse onset time in us
stimOptions.pw = 25;               % pulse width in us
stimOptions.IPG = 8;               % interphase gap in us
stimOptions.type = 'af';
allStims = cell(length(I),1);

for curIDX = 1: length(I)
    stimOptions.I = I(curIDX);
    stimOptsCell = struct2varargin(stimOptions);
    allStims{curIDX} = makeStim_SinglePulse(stimOptsCell{:});
end

% Save all the stimulus values after creating allStims.
stimOptions.I = I; %#ok<STRNU>
%% Calculate segment properties from fiber and model info
segments = fiber_initialization(fiber,model);

%% Run model
% Need consistent field order for MEX function.
segments = orderfields(segments);
model = orderfields(model);
options = orderfields(options);
spikeOutput = cell(length(I),1);
vOutput = cell(length(I),1);

tic;
for curIDX = 1:length(I)
    if options.recV
        [spikeOutput{curIDX},vOutput{curIDX}] = stochAN_multi_mex(allStims{curIDX},segments,model,options);
    else
        spikeOutput{curIDX} = stochAN_multi_mex(allStims{curIDX},segments,model,options);
    end
end
toc;

cd(dataDir);
save(exptName,'spikeOutput','vOutput','stims','options','model','fiber','stimOptions');

