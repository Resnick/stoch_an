function [spikeOutput,vOutput] = paired_Run(condI,testI,IPI)
%% Set model properties
model = model_Config; % Function with model parameters in it.

%% Set fiber properties
diamMean = 1.447;
fiber.diameter = diamMean;      % axolemma diameter (um)
fiber.dtoD = 0.643;             % d/D
% fiber.cutoff = [];            % PLACEHOLDER: demyelination extent
% fiber.demydtoD = [];          % PLACEHOLDER: demyelination severity

fiber.numNodes = 36;
fiber.zDistance = 3;            % distance of fiber from electrode center (mm)
fiber.intSegs = ceil(model.intSegs*fiber.diameter/diamMeant);

%% Set experimental options
options = options_Config();
options.recV = 1;
options.XstimLoc = -(model.LtoD*diamMean*1e-3/...
    model.normdtoD+model.nLen)*options.meanNodeAbove;

%% Generate stimulus
stimOptions.condI = condI;
stimOptions.testI = testI;
stimOptions.IPI = IPI;
stimOptions.wait = 2000;            % simulation time in us
stimOptions.delay = 1000;           % pulse onset time in us
stimOptions.pw = 25;                % pulse width in us
stimOptions.IPG = 8;                % interphase gap in us
stimOptions.type = 'cf';

stimOptsCell = struct2varargin(stimOptions);
Istim = makeStim_PairedPulse(stimOptsCell{:});
simtime = length(Istim);

%% Calculate segment properties from fiber and model info 
segments = fiber_initialization(fiber,model);

%% Run model
    % Need consistent field order for MEX function.
    segments = orderfields(segments);
    model = orderfields(model);
    options = orderfields(options);
tic;
if options.recV
    [spikeOutput,vOutput] = stochAN_multi_mex(Istim,segments,model,options);
else
    spikeOutput = stochAN_multi_mex(Istim,segments,model,options);
end
toc;

%% Plot voltage data
if options.recV
    figure
    imagesc(squeeze(vOutput(end,:,:)-84));
    c = colorbar; c.Label.String = 'Voltage (mV)';
    xlabel('Segment #');
    ylabel('Timestep (\mus)');
    
    dt = model.dt;
    figure
    subplot(2,1,1)
    for i = 1:30:360
        plot(0:options.Vsample/dt:simtime-1,vOutput(1,:,i)-84); hold on;
    end
    subplot(2,1,2)
    plot(0:1:simtime-1,Istim,'k','LineWidth',2)
end


%% Plot spiking data
recSpikes = squeeze(spikeOutput(:,32,:));
spikeTimes = mat2cell(recSpikes,ones(1,options.numMonte),options.maxSpikes);

figure
subplot(2,1,1)
LineFormat.Color = 'k'; LineFormat.LineWidth = 1.5;
plotSpikeRaster(spikeTimes,'PlotType','vertline','XLimForCell',...
    [0,simtime*1000],'LineFormat',LineFormat);
ylabel('Node Number');
xlabel('Spike Time (\mus)'); xlim([0,simtime]);
subplot(2,1,2)
plot(0:1:simtime-1,Istim,'k','LineWidth',2)
end