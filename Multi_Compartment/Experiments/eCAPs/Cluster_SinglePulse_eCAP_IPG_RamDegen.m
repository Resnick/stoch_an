function Cluster_SinglePulse_eCAP_IPG_RamDegen(ExptName,dataDir,codeDir)

%% Population set-up
% Set Population Moment Parameters based on data
% from Dyan Ramekers
diamMeans	= [exp(0.5944), exp(0.4194) ,exp(0.2921)];
diamStdevs	= [0.2433, 0.2327 ,0.2405 ];

% dtoD of healthy neurons. 0.643 Wan & Corfas 2017
normdtoDmu      = 0.643;
normdtoDsigma   = 0.0738;

numFibers = 200;
%% Set fiber-independent model properties
model   = model_Config;
model.Terminal.areaCoef = 20;

%% Initialize fiber populations

numPops = length(diamMeans);
fibPop = cell(numPops,1);

for popIDX = 1:numPops
    anamProps(1) = diamMeans(popIDX);
    anamProps(2) = diamStdevs(popIDX);
    anamProps(3) = normdtoDmu;
    anamProps(4) = normdtoDsigma;
    
    tempFibs = initialize_Population(numFibers,anamProps,model);
    fibPop{popIDX} = tempFibs;
end

%% Set experimental options
options = options_Config;
options.numMonte = 5;
options.maxSpikes = 20;
options.recV = 0;
options.recECAP = 1;

%% Generate stimuli
I0 = 0.5;
Imin = 1; %
Imax = 15;
numI = 35;
stimOptions.pw = 25;                                % pulse width in us
stimOptions.wait = 2000;                % simulation time in us
stimOptions.delay = 2000;               % pulse onset time in us
IPG = [2,10,20,30,40];    % interphase gap in us
stimOptions.type = 'cf';
allStims = cell(length(IPG),numI+1);
I = zeros(length(IPG),numI+1);

for ipgIDX = 1: length(IPG)
    stimOptions.IPG = IPG(ipgIDX);
    I(ipgIDX,1) = I0;
    I(ipgIDX,2:end) = logspace(log10(Imin),log10(Imax),numI);
    for curIDX = 1: length(I)
        stimOptions.I = I(ipgIDX,curIDX);
        stimOptsCell = struct2varargin(stimOptions);
        allStims{ipgIDX,curIDX} = makeStim_SinglePulse(stimOptsCell{:});
    end
end

% Save all the stimulus values after creating allStims.
stimOptions.I = I; 
stimOptions.IPG = IPG; %#ok<STRNU>
if ~ispc
    % If on unix system save the git hash from the system
    [~,git_hash] = system('git rev-parse HEAD');  %#ok<ASGLU>
end
%% Save overall experiment info
cd(dataDir);
if ~isfolder(ExptName)
    mkdir(ExptName);
end
cd(ExptName); ExptFolder = pwd;
ExptFile = sprintf('%s%sExpt.mat',ExptFolder,filesep);

if ~ispc
    save(ExptFile,'fibPop','allStims','model','options','stimOptions','git_hash');
else
    save(ExptFile,'fibPop','allStims','model','options','stimOptions');
end

cluster_Expt_Setup(ExptName,codeDir,fibPop,allStims,options,ExptFolder)
