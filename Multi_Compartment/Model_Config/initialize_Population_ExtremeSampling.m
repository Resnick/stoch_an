function fiberPop = initialize_Population_ExtremeSampling(numFibers,anamProps,model)
%INITIALIZE_Population: This function recieves and integer containing the
%number of fibers and structure containing the statistical parameters for
%population fiber anatomy/geometry. It returns fiberPop, a structure
%containing arrays structural properties for all individual fibers.
%
% Usage: fiberPop = initialize_Population(numFibers,anamProps)
%
%   fiberPop:       Structure containing arrays of fiber individual fibers'
%                   properties.
%   numFibers:      Integer number of fibers to creates
%   anamProps:      Anatomical property statistics for generating 
%                   populations.
%
% Jesse M. Resnick (resnick@uw.edu) � 2018

%%  Set Constant Anatomic Parameters
diamMean	= anamProps(1);	% use 2 mm Imennov & Rubinstein 2009. 1.477 mm Wan & Corfas 2017
diamStdev	= anamProps(2);   % use 0.5 mm Imennov & Rubinstein 2009. 0.22 mm Wan & Corfas 2017
normdtoDmu    = anamProps(3);     %dtoD of healthy neurons. 0.643 Wan & Corfas 2017
normdtoDsigma = anamProps(4);

diameters(1:3) = diamMean-2*diamStdev; % Extreme sampling of the fiber population's diameters
diameters(4:6) = diamMean; 
diameters(7:9) = diamMean+2*diamStdev; 

zs = ones(1,numFibers)*3; % Electrode distance from fiber. (mm)
numnodes = ceil(36./(diameters./(diamMean)));  % the number of active nodes in the nerve
intSegs = ceil(model.intSegs*diameters/diamMean);
maxNodes = max(numnodes);

%% Set myelination state for each fiber
% Uses  normpdf to pull replicate variance in fiber myelination observed in
% mouse SGNS in Wan and Corfas, 2017
dtoD = [normdtoDmu-2*normdtoDsigma normdtoDmu normdtoDmu+2*normdtoDsigma]; % Extreme sampling of the fiber population's dtoD
dtoD = repmat(dtoD,1,3);


%% -----------------------------------------------------------------------
% SAVE EXPERIMENT CONFIGURATION INFORMATION to Struct
%-------------------------------------------------------------------------
fiberPop.zs             = zs;
fiberPop.maxNodes       = maxNodes;
fiberPop.diameters		= diameters;
fiberPop.numNodes       = numnodes;
fiberPop.dtoD           = dtoD;
fiberPop.intSegs        = intSegs;
fiberPop.diamMean       = diamMean;
fiberPop.diamStdev      = diamStdev;
fiberPop.normdtoDmu     = normdtoDmu;
fiberPop.normdtoDsigma  = normdtoDsigma;
end