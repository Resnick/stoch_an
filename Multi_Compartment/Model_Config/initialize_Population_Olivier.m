function fiberPop = initialize_Population_Olivier(anamProps,model)
%INITIALIZE_Population: This function recieves and integer containing the
%number of fibers and structure containing the statistical parameters for
%population fiber anatomy/geometry. It returns fiberPop, a structure
%containing arrays structural properties for all individual fibers.
%
% Usage: fiberPop = initialize_Population(numFibers,anamProps)
%
%   fiberPop:       Structure containing arrays of fiber individual fibers'
%                   properties.
%   numFibers:      Integer number of fibers to creates
%   anamProps:      Anatomical property statistics for generating 
%                   populations.
%
% Jesse M. Resnick (resnick@uw.edu) � 2018

%%  Set Constant Anatomic Parameters
diamMean	= 1.5;	% Fiber with diamMean=1.5 mm has 36 nodes - to be consistent with previous Jay's experiments

[zs, diameters, dtoD] = handpick_Population(anamProps);

numnodes = ceil(36./(diameters./(diamMean)));  % the number of active nodes in the nerve
intSegs  = ceil(model.intSegs.*diameters/diamMean);
maxNodes = max(numnodes);


%% -----------------------------------------------------------------------
% SAVE EXPERIMENT CONFIGURATION INFORMATION to Struct
%-------------------------------------------------------------------------
% Save generated values
fiberPop.zs             = zs';
fiberPop.maxNodes       = maxNodes;
fiberPop.diameters		= diameters';
fiberPop.numNodes       = numnodes';
fiberPop.dtoD           = dtoD';
fiberPop.intSegs        = intSegs';

% Save selected anatomical values
fiberPop.diamMean       = diamMean;

end