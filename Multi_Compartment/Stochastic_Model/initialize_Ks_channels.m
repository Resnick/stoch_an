function numKs = initialize_Ks_channels(ratesKs,numK,dt)
% NUMKS: This function initializes the number of channels in each state
% using a Jump Monte Carlo method.
%
% Usage: numKs = initialize_Ks_channels(ratesKs,numK,dt)
%
%   numKs:      Array of # of channels in each state
%   ratesKs:    Voltage dependent transition rates
%   numK:       Number of K channels at segment
%   dt:         Timestep (us)
%
% Jesse M. Resnick (resnick@uw.edu) � 2018

% initial destribution of particles in each channel
numKs = round([158,8]*numK/168);

settletime = 100;

% Net transition rates for each channel type.
zeta = [ratesKs(1); ratesKs(2)];

Tl = 0;

% Indices of channel types for different reaction types.
p_trans_curr = [1,2];
p_trans_next = [2,1];

while   (Tl<=settletime*dt)
    
    lambda = numKs*zeta;
    
    tl = -log(rand) / lambda;
    Tl = Tl + tl;
    
    if (Tl>settletime*dt)
        break;
    end
    
    P = cumsum([ratesKs(1)*numKs(1),...
        ratesKs(2)*numKs(2)...
        ]/lambda);
    
    ind = find(rand<P,1);
    
    numKs(p_trans_curr(ind)) = numKs(p_trans_curr(ind)) - 1;
    numKs(p_trans_next(ind)) = numKs(p_trans_next(ind)) + 1;
end

end

