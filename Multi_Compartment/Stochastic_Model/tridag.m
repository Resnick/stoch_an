function Vnext = tridag(a,b,c,rhs)
% TRIDAG solves the system of equations A*Vnext = rhs. Where Vnext and rhs
% are column vectors and A is a tridiagonal matrix of the form:
%       [b(1)   c(1)    0       ...     0       0       0
%       a(2)    b(2)    c(2)    0       ...     0       0
%       0       a(3)    b(3)    c(3)    0       ...     0
%       0       0       ...     ...     ...     ...     0
%       0       0       ...     ...     ...     ...     0
%       0       0       ...     ...     a(n-1)  b(n-1)  c(n-1)
%       0       0       0       ...     ...     a(n)    b(n)] 
%
% Jesse M. Resnick (resnick@uw.edu) � 2018

numSegs = length(rhs);
Vnext = zeros(1,numSegs);
gamma = zeros(1,numSegs);
beta = b(1);
Vnext(1) = rhs(1)/beta;

% Decomposition and forward subsitution phase
for segIDX = 2:numSegs
    gamma(segIDX) = c(segIDX-1)/beta;
    beta = b(segIDX)-a(segIDX)*gamma(segIDX);
    Vnext(segIDX) = (rhs(segIDX)-a(segIDX)*Vnext(segIDX-1))/beta;
end

% backward substituion phase
for segIDX = numSegs-1:-1:1
    Vnext(segIDX) = Vnext(segIDX)-gamma(segIDX+1)*Vnext(segIDX+1);
end

end

