function ratesNaf = trans_rates_Naf(V,Kins)
% trans_rates_Naf: This function calculates the potassium channel transition
% rates for a given voltage.
%
% Usage: ratesNaf = trans_rates_Naf(V,Kins);
%
%   ratesNaf:    Voltage dependent transition rates 
%   V:          Segment membrane potential (mV)
%   Kins:       Array containing kinematic parameters.
%
% Jesse M. Resnick (resnick@uw.edu) � 2018

ratesNaf = zeros(4,length(V));
ratesNaf(1,:) = Kins(1) * (V - Kins(2)) ./...       % a_m
    (1 - exp((Kins(2) - V) / Kins(3)));           
ratesNaf(2,:) = Kins(4) * (Kins(5) - V) ./...       % b_m
    (1 - exp((V - Kins(5)) / Kins(6)));
ratesNaf(3,:) = Kins(7) * (Kins(8) - V) ./...       % a_h
    (1 - exp((V - Kins(8)) / Kins(9)));
ratesNaf(4,:) = Kins(10) ./...                      % b_h
    (1 + exp((Kins(11) - V) / Kins(12)));
end

