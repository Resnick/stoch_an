function numKf = initialize_Kf_channels(ratesKf,numK,dt)
% NUMKF: This function initializes the number of channels in each state
% using a Jump Monte Carlo method.
%
% Usage: numKf = initialize_Kf_channels(ratesKf,numK,dt)
%
%   numKf:      Array of # of channels in each state
%   ratesKf:    Voltage dependent transition rates
%   numK:       Number of K channels at segment
%   dt:         Timestep (us)
%
% Jesse M. Resnick (resnick@uw.edu) � 2018

% initial destribution of particles in each channel
numKf = round([158,  8,  0,  0,  0]*numK/168);

settletime = 100;

% Net transition rates for each channel type.
zeta = [4*ratesKf(1);...
    3*ratesKf(1) + ratesKf(2);...
    2*ratesKf(1) + 2*ratesKf(2);...
    ratesKf(1) + 3*ratesKf(2);...
    4*ratesKf(2)];

Tl = 0;

% Indices of channel types for different reaction types.
p_trans_curr = [1,2,2,3,3,4,4,5];
p_trans_next = [2,1,3,2,4,3,5,4];

while   (Tl<=settletime*dt)
    
    lambda = numKf*zeta;
    
    tl = -log(rand) / lambda;
    Tl = Tl + tl;
    
    if (Tl>settletime*dt)
        break;
    end
    
    P = cumsum([4*ratesKf(1)*numKf(1),...
        ratesKf(2)*numKf(2),...
        3*ratesKf(1)*numKf(2),...
        2*ratesKf(2)*numKf(3),...
        2*ratesKf(1)*numKf(3),...
        3*ratesKf(2)*numKf(4),...
        ratesKf(1)*numKf(4),...
        4*ratesKf(2)*numKf(5)...
        ]/lambda);
    
    ind = find(rand<P,1);
    
    numKf(p_trans_curr(ind)) = numKf(p_trans_curr(ind)) - 1;
    numKf(p_trans_next(ind)) = numKf(p_trans_next(ind)) + 1;
end

end

