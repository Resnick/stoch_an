
stimOptions.condI = 0;
% stimOptions.testI = 10; %% usually this says I0, changing the number
                            % directly here for troubleshooting.
stimOptions.IPI   = 1000;
stimOptions.wait = 2000;           % simulation time in us
stimOptions.delay = 2000;           % pulse onset time in us
stimOptions.pw = 25;               % pulse width in us
stimOptions.IPG = 8;               % interphase gap in us
stimOptions.type = 'cf';

% Calling the stimulating current functions
stimOptsCell = struct2varargin(stimOptions);
Istim = makeStim_PrePulseBal();
plot(Istim)