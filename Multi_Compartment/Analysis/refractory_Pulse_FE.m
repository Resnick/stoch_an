function [fiberFE,popFE] = refractory_Pulse_FE(spikeNodeData)
%rEFRACTORY_PULSE_FE Calculates the fiber and population firing
%efficiencies for the test pulse of a paired pulse experiment.

numFibers   = length(spikeNodeData);
numMonte    = size(spikeNodeData{1},1);
testSpikes = nan(numFibers,1);

for fibIDX = 1:numFibers
    didSpike = isfinite(spikeNodeData{fibIDX});
    singleMonteSpikes   = sum(didSpike,2);
    if any(singleMonteSpikes > 2)
        warning('More than 2 spikes on at least 1 monte carlo trial.');
    elseif any(singleMonteSpikes < 1)
        warning('Pre-pulse did not initiate spike on at least 1 monte carlo trial.');
    end
    testSpikes(fibIDX)         = sum(didSpike(:,2));
end

fiberFE = testSpikes/numMonte;
popFE   = sum(testSpikes)/(numFibers*numMonte);
