function [latency, jitter] = LatJitCompute(expt)

jitter = zeros(1, expt.idx2Max);
latency = zeros(1, expt.idx2Max);

% Extract the relevant spike times from the expt structure
for i = 1:expt.idx2Max; % Loop over each stimulus intensity
        
        tmp_spks = cell2mat(expt.trial{i}.spikenode);
            % you have to convert cells to a matrix here
        
        if isempty(tmp_spks)
            jitter(i) = NaN;
            latency(i) = NaN;
        else
            %how long was the delay before the stimulus onset?
            delay = expt.trial{i}.stim.pulselist(1,2)*1e-3;
            tmp_spks = tmp_spks - delay;
            latency(i) = nanmean(tmp_spks);
            jitter(i) = nanstd(tmp_spks);
        end
end

end