function [fiberFE,popFE,spikeNodeData] = Single_Pulse_PWExpt_Process(allStims,fibPop,model,options,stimOptions,allSpikeOut)

spikeData = reshape(allSpikeOut,size(allStims));
spikeNodeData = cell(size(allStims));
popFE = zeros(size(allStims));
numFibers = length(fibPop{1}.diameters);
fiberFE = zeros([size(allStims) numFibers]);
for stimIDX = 1:numel(spikeData)
    spikeNodeData{stimIDX} = recordingNodeSpikes(spikeData{stimIDX},fibPop{1},model,options);
    [I,J] = ind2sub(size(spikeData),stimIDX);
    [fiberFE(I,J,:),popFE(stimIDX)] = single_Pulse_FE(spikeNodeData{stimIDX});
end
dbStims = 20*log10(stimOptions.I/0.001);

for pwIDX = 1:size(fiberFE,1)
    [fiberThresh(pwIDX,:),fiberRS(pwIDX,:)] = CDF_thresh_fit(fiberFE(pwIDX,:,:),stimOptions.I(pwIDX,:));
    [popthresh(pwIDX),popRS(pwIDX)] = CDF_thresh_fit(popFE(pwIDX,:),dbStims(pwIDX,:));
end
    
for pwIDX = 1:size(fiberFE,1)
    plot(dbStims(pwIDX,:),popFE(pwIDX,:),'LineWidth',1.5); hold on;
end
leghand = legend('12.5','25','50','100','200','400','Location','southeast');
xlabel('Stimulus Current (dB)'); xlim([-inf,inf])
ylabel('Firining Efficiency'); 
title(leghand,{'Phase Duration (\mus)'})

targetFE = 0.5;
[fe50,IDXfe50] = min(abs(popFE-targetFE),[],2);
fe50 = fe50 + targetFE;
for pwIDX = 1:size(fiberFE,1)
    temp = spikeNodeData{pwIDX,IDXfe50(pwIDX)};
    for fibIDX = 1:numFibers
        lats50(pwIDX,fibIDX,:) = temp{fibIDX}(:,1);
    end
end
lats50 = reshape(lats50,size(lats50,1),size(lats50,2)*size(lats50,3));
meanlat50 = mean(lats50,2,'omitnan');
jitter50 = std(lats50,[],2,'omitnan');
clear lats50

targetI = popThresh-2*popRS;
[delta,targetIDX] = min(abs(dbStims-repmat(targetI.',1,100)),[],2);
if delta./targetI > 0.05, warning('I/O curve poorly sampled there'); end
for pwIDX = 1:size(fiberFE,1)
    temp = spikeNodeData{pwIDX,targetIDX(pwIDX)};
    for fibIDX = 1:numFibers
        temp2(pwIDX,fibIDX,:) = temp{fibIDX}(:,1);
    end
end
latsTarg = reshape(temp2,size(temp2,1),size(temp2,2)*size(temp2,3));
meanlatTarg = mean(latsTarg,2,'omitnan');
jitterTarg = std(latsTarg,[],2,'omitnan');

histogram(latsTarg(1,:),'NumBins',100,'BinLimits',[500,1700]); hold on
histogram(latsTarg(6,:),'NumBins',100,'BinLimits',[500,1700]);
xlabel('Latency')

for stimIDX = [1:10:61,66]
    spikeTimes = [];
    for fibIDX =  1:numFibers
        spikeTimes = [spikeTimes, spikeNodeData{stimIDX}{fibIDX}];
    end
    figure
    histogram(spikeTimes,'BinLimits',[500,1120],'NumBin',50)
end

logStims = 20*log10(stimOptions.I/0.001);
for fibIDX = 1:numFibers
    plot(logStims,fiberFE(:,1,fibIDX),'LineWidth',1.5); hold on
end
ylabel('Firing Efficiency'); ylim([-.1,1.1])
xlabel('Stimulus Current (db re 1\muA)');xlim([-inf,inf])