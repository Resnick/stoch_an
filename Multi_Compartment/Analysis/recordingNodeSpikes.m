function [spikeNodeData,options] = recordingNodeSpikes(spikesData,Fibers,model,options)
%RECORDING_NODE_SPIKES This function accepts the spikesData for a batch of
%fibers and extracts the spike time data at the recording node of each
%fiber.

numFibers = numel(spikesData);
diamMean = Fibers.diamMean;
options.XrecordLoc = -(model.LtoD*diamMean*1e-3/...
    model.normdtoD+model.nLen)*options.meanRecordNode;
options.recordNodes = round(options.XrecordLoc./-(model.LtoD.*Fibers.diameters*1e-3/...
    model.normdtoD+model.nLen));
spikeNodeData = cell(numFibers,1);

for fibIDX = 1:numFibers
    thisRecordNode = options.recordNodes(fibIDX);
    spikeNodeData{fibIDX} = squeeze(spikesData{fibIDX}(:,thisRecordNode,:));
end

end

