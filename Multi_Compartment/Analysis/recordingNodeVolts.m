function voltNodeData = recordingNodeVolts(voltsData,Fibers,model,options)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

numFibers = length(voltsData);
diamMean = mean(Fibers.anamProps(1));
options.XrecordLoc = -(model.LtoD*diamMean*1e-3/...
    model.normdtoD+model.nLen)*options.meanRecordNode;
recordNodes = round(options.XrecordLoc./-(model.LtoD.*Fibers.diameters*1e-3/...
    model.normdtoD+model.nLen));
voltNodeData = cell(numFibers,1);

for fibIDX = 1:numFibers
    recordSegment = recordNodes(fibIDX)*10;
    voltNodeData{fibIDX} = squeeze(voltsData{fibIDX}(:,:,recordSegment));
end

end

