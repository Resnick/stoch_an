% This script makes a histogram of which node the spike is initiated at. We
% define initiation as the node which spikes first. 
function nList = Spike_Initiation(expt)

% Find the closest to 50% Firing Efficiency
[FE, ~,~] = FEcompute(expt);
f50 = getclosest(FE,0.5);

spks = expt.trial{f50}.spikes{1};

% First make sure there are no double spikes, which occasionally happens.
cardinality = cellfun('length', spks);
[a,b] = find(cardinality > 1);

if ~isempty(a)
    for j = 1:length(a)
        spks{a(j),b(j)} = spks{a(j),b(j)}(1);
    end
end

% Replace empty cells with 0 for easy reading
emptyIndex = cellfun(@isempty, spks);
spks(emptyIndex) = {0};
M = cellfun(@(x) double(x), spks);

% For every mcr trial, find the node at which the spike time is minimal
 for j = 1:length(M)
     tmp = M(:,j);
     [~,node] = min(tmp(tmp~=0));
     
     if isempty(node) % If there are no spike times to report, initiation is n/a
         nList(j) = NaN;
     else
     nList(j) = node;
     end
 end
end
 %figure
%hist(nList, 1:36);
%set(get(gca,'child'),'FaceColor','r','EdgeColor','k')
