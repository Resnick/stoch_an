function [maxtab] = peak_Det(v,level)

% peakdet_ians detects the peaks in a train of spikes. The function was
% designed specifically for action potential detection, where it has
% one unique maximum point at each AP spike peak.
% 
% [maxtab] = peakdet(v,level)
% 
% maxtab is the output matrix, composed of two columns, the first column
% for the position and the other for the value of the peak.
% v is the input vector
% level is the minimum detection level of interest
% 
% Ian C. Bruce (ibruce@ieee.org) � 2009

v = v(:);
index = find(v<level);
v(index(:)) = 0;
crossings = diff(v>0);
crossind = find(abs(crossings)>0);

if rem(length(crossind),2)~=0
    error('Could not find even number of level crossings to detect peaks')
end

numcrossings = length(crossind)/2;

maxtab = zeros(numcrossings,2);

for lp = 1:numcrossings
    
    [maxval,ind] = max(v(crossind(2*lp-1):crossind(2*lp)));
    maxtab(lp,:) = [crossind(2*lp-1)+ind-1, maxval];
    
end