function FE = twopulsesim(I0,IPI,pw,gap,simTime,nruns,tempfilename)

% FE = twopulsesim(I0,IPI,pw,gap,simTime,nruns,tempfilename)
%                                                                          
% twopulsesim returns the firing efficiency for a pair of current pulses
%   I0 gives the stimulus current amplitudes for the two pulses in pico Ampere (pA)
%   IPI is the inter-pulse interval in milliseconds (ms)
%   pw is the phase width in microsecond (us)                       
%   gap is the intergap puls in microsecond (us)
%   simTime is the simulation period in milliseconds (ms)
%   nruns is the number of simulation repetitions (trials)
%   tempfilename is the string of the returned MAT file
%
% Mohamed H. Negm (negmmh@mcmaster.ca) & Ian C. Bruce (ibruce@ieee.org)
% � 2008-2014

ANmodel = @stochAN_public1p0;
spike = zeros(1,nruns);
lat = zeros(1,nruns);
spikeCount = zeros(1,nruns);

dt = 1e-3;
t = 0:dt:simTime;
k_max = length(t);
leadtime = 0.1;
Istim = zeros(1,k_max);
spikewaveforms = [];

% Calling the stimulating current functions 
Istim(round(leadtime/dt+1):round((leadtime+pw*1e-3)/dt)) = 1e-6*I0(1);
Istim(round((leadtime+(pw+gap)*1e-3)/dt+1):round((leadtime+(2*pw+gap)*1e-3)/dt)) = -1e-6*I0(1);
Istim(round((leadtime+IPI)/dt+1):round((leadtime+IPI+pw*1e-3)/dt)) = 1e-6*I0(2);
Istim(round((leadtime+IPI+(pw+gap)*1e-3)/dt+1):round((leadtime+IPI+(2*pw+gap)*1e-3)/dt)) = -1e-6*I0(2);

spikeIndex = [];
for lp=1:nruns
    disp(['Run ' int2str(lp) '/' int2str(nruns)])
    v = ANmodel(Istim);
    spike(lp) = max(v) > 80;
    if spike(lp)                  % check if at least one spike occured
        [getspike] = peakdet(v,80);   % call the peak detection function
        latfirst = t(getspike(1,1));    % latency of the first spike
        lat(lp) = latfirst;
        ampAvg(lp) = mean(getspike(:,2)); % averaged amplitude of all spikes
        spikeCount(lp) = size(getspike,1);
        spikeIndex = [spikeIndex ; getspike(:,1)];
        if isempty(spikewaveforms)
            spikewaveforms = v;
        else
            spikewaveforms = [spikewaveforms; v];
        end
    else
        lat(lp) = NaN;
        ampAvg(lp) = NaN;
        spikeCount(lp) = 0;
    end

    save(tempfilename);

end

FE = sum(spikeCount)/nruns;
% if FE > 0
if sum(spike) > 0
    spikeAvg = mean(spikeCount(find(~isnan(spikeCount))))*1000/simTime;
    spikeVar = var(spikeCount(find(~isnan(spikeCount))));
    LT       = mean(lat(find(~isnan(lat))));
    JT       = std(lat(find(~isnan(lat))));
    Amp      = mean(ampAvg(find(~isnan(ampAvg))));
    
else
    spikeAvg = NaN;
    LT       = NaN;
    JT       = NaN;
    Amp      = NaN;
end

avgspikewaveform = mean(spikewaveforms);

save(tempfilename);