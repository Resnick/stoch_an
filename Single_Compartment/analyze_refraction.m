% Mohamed H. Negm (negmmh@mcmaster.ca) & Ian C. Bruce (ibruce@ieee.org)
% � 2008-2014

global gh glt

% Parameters for HH model
% filename = 'refraction_results_HH';
% gh  = 0;
% glt = 0;
 
% Parameters for Ih model
% filename = 'refraction_results_Ih';
% gh  = 13e-9;
% glt = 0;

% Parameters for IKLT model
% filename = 'refraction_results_IKLT';
% gh  = 0;
% glt = 13e-9;

% Parameters for Ih & IKLT model
filename = 'refraction_results_IhIKLT';
gh  = 13e-9;
glt = 13e-9;

% Note that the following stimulus parameters are different to those used
% in Negm & Bruce IEEE TBME, to reduce the run time.  To replicate the
% results from the journal paper, increase nruns to 100 and numIlevels to
% 30.
nruns = 20;
numIlevels = 10;

Ipulse1 = 50;
pw = 75;
gap = 75;

% Use the following line for a quicker estimate of the refractory function
numIPIs = 15; IPIs = [linspace(2*(pw+gap)*1e-3,0.7,10) logspace(log10(0.75),1,numIPIs-10)];

% Use the following lines to match the IPIs used in Negm & Bruce IEEE TBME
% IPIs = [0.3000    0.3100    0.3414    0.3729    0.3854    0.4043    0.4357    0.4671    0.4951    0.4986...
%         0.5300    0.5614    0.5929    0.6243    0.6360    0.6557    0.6871    0.7186    0.7500    0.8170...
%         1.0496    1.3483    1.7321    2.2250    2.8583    3.6719    4.7170    6.0596    7.7844   10.0000]; numIPIs = length(IPIs);

if (gh > 0)&&(glt > 0)
    Ilevels = linspace(25,75,numIlevels); % Suitable for IKLT + Ih model
elseif (gh > 0)||(glt > 0)
    Ilevels = linspace(20,75,numIlevels); % Suitable for IKLT model or Ih model
else
    Ilevels = linspace(20,50,numIlevels); % Suitable for HH model
end

FE_pulse2 = zeros(numIPIs,numIlevels);
Ith = zeros(numIPIs,1);
RS = zeros(numIPIs,1);

tempresultsfilename = 'analyze_refraction_temp_results';

for IPIlp = 1:numIPIs
   
   IPI = IPIs(IPIlp);
   
   for Ilp = 1:numIlevels
      
       disp(['analyze_refraction - IPIlp = ' num2str(IPIlp) '/' num2str(numIPIs) '; Ilp = ' num2str(Ilp) '/' num2str(numIlevels)])
       
       Ipulse2 = Ilevels(Ilp);
       
       FE = twopulsesim([Ipulse1 Ipulse2],IPI,pw,gap,2*IPI+2,nruns,tempresultsfilename);
              
       FE_pulse2(IPIlp,Ilp) = FE - 1; % Calculate the FE for just the second pulse
           
   end

   X = fminsearch(@(x) intgausserr(x,Ilevels,FE_pulse2(IPIlp,:)),[mean(Ilevels) 0.03*mean(Ilevels)]);

   Ith(IPIlp) = X(1);
   RS(IPIlp) = X(2)/X(1);
   
   save(filename)

end

save(filename)

plot_refractory_fit

