function FE = one_Pulse_Sim(I0,pulseonset,pw,gap,simtime,nruns,tempfilename)

% FE = onepulsesim(I0,pulseonset,pw,gap,simtime,nruns,tempfilename)
%
% onepulsesim returns the firing efficiency for a single current pulse
%   I0 gives the stimulus current amplitude for a single biphasic pulse in pico Ampere (pA)
%   pulseonset is the time at which the stimulating pulse is applied in milliseconds (ms)
%   pw is the phase width in microsecond (us)
%   gap is the inter-phase gap in microsecond (us)
%   simtime is the simulation period in milliseconds (ms)
%   nruns is the number of simulation repetitions (trials)
%   tempfilename is the string of the returned MAT file
%
% Mohamed H. Negm (negmmh@mcmaster.ca) & Ian C. Bruce (ibruce@ieee.org)
% � 2008-2014

ANmodel = @stochAN;
spike = zeros(1,nruns);
lat = zeros(1,nruns);
spikeCount = zeros(1,nruns);

dt = 1e-3;
t = 0:dt:simtime;
k_max = length(t);
Istim = zeros(1,k_max);
spikewaveforms = [];

% Calling the stimulating current functions
Istim(round(pulseonset/dt+1):round((pulseonset+pw*1e-3)/dt)) = 1e-6*I0;
Istim(round((pulseonset+(pw+gap)*1e-3)/dt+1):round((pulseonset+(2*pw+gap)*1e-3)/dt)) = -1e-6*I0;

spikeIndex = [];
for lp=1:nruns
    tic;
    disp(['Run ' int2str(lp) '/' int2str(nruns)])
    v = ANmodel(Istim);
    spike(lp) = max(v) > 80;
    if spike(lp)                  % check if at least one spike occured
        [getspike] = peakdet(v,80);   % call the peak detection function
        latfirst = t(getspike(1,1));    % latency of the first spike
        lat(lp) = latfirst;
        ampAvg(lp) = mean(getspike(:,2)); % averaged amplitude of all spikes
        spikeCount(lp) = size(getspike,1);
        spikeIndex = [spikeIndex ; getspike(:,1)];
        if isempty(spikewaveforms)
            spikewaveforms = v;
        else
            spikewaveforms = [spikewaveforms; v];
        end
    else
        lat(lp) = NaN;
        ampAvg(lp) = NaN;
        spikeCount(lp) = 0;
    end

    save(tempfilename);
    toc
end

FE = sum(spikeCount)/nruns;
if sum(spike) > 0
    spikeAvg = mean(spikeCount(find(~isnan(spikeCount))))*1000/simtime;
    spikeVar = var(spikeCount(find(~isnan(spikeCount))));
    LT       = mean(lat(find(~isnan(lat))));
    JT       = std(lat(find(~isnan(lat))));
    Amp      = mean(ampAvg(find(~isnan(ampAvg))));

else
    spikeAvg = NaN;
    LT       = NaN;
    JT       = NaN;
    Amp      = NaN;
end

if ~isempty(spikewaveforms)

    avgspikewaveform = mean(spikewaveforms);

end

save(tempfilename);