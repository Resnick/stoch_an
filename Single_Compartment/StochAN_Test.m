tic;

options.dt = 1e-3;              % sim timestep in ms
options.E_Na = 66;              % reverse potenial for sodium in mV
options.E_K = -88;              % " potassium "
options.E_h = -43;              % " hyperpolarization-activated cation "

options.Vrest =-78;             % resting membrane potential for Na and Kir equ.
options.Vrest2 =-63.6;          % resting membrane potential for h and Klt equ.

options.gNa = 25.69e-9;         % single channel conductence for sodium in mS
options.gK = 50.0e-9;           % single channel conductence for Kir in mS
options.gh = 13e-9;             % single channel conductence for HCN in mS
options.glt = 13e-9;            % single channel conductence for KLT in mS

% Maximum number of channels of each type
options.N_Na_max = 1000;
options.N_K_max = 166;
options.N_lt_max = 166;
options.N_h_max = 100;
options.seed = sum(100*clock);

% Set fiber properties
fiber.Cm = .0714e-6;        % node capacitance
fiber.Rm = 1953.49e3;       % node resistance

% Generate stimulus
dt = options.dt;
simtime = 1000;             % simulation time in ms
t = 0:dt:simtime;
k_max = length(t);
pulseonset = 100;           % pulse onset time in ms
pw = 100;                   % pulse width in us
gap = 100;                  % interphase gap in us
I0 = 5;                     % peak current intensity
Istim = zeros(1,k_max);

% Calling the stimulating current functions
Istim(round(pulseonset/dt+1):round((pulseonset+pw*1e-3)/dt)) = 1e-6*I0;
Istim(round((pulseonset+(pw+gap)*1e-3)/dt+1):round((pulseonset+(2*pw+gap)*1e-3)/dt)) = -1e-6*I0;

[v,N_Na,N_K,N_h,N_lt] = stochAN_single(Istim,fiber,options);

toc;