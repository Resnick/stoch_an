% Create a logical image volume of with specified image size.
% First create the image.
imageSizeX = 100;
imageSizeY = 100;
imageSizeZ = 100;
[columnsInImage, rowsInImage, pagesInImage] = meshgrid(1:imageSizeX, 1:imageSizeY,1:imageSizeZ);

% Next create the disc in the image.
centerX = 60;
centerY = 60;
centerZ = 50;
radius = 30;
discVoxels = (rowsInImage - centerY).^2 ...
    + (columnsInImage - centerX).^2  <= radius.^2 & + (pagesInImage - centerZ) == 0;

% discVoxels is a 3D "logical" array.
% Now, display it using an isosurface and a patch
fv = isosurface(discVoxels,0);
patch(fv,'FaceColor',[0 0 0],'EdgeColor',[0 0 0]); hold on;
view(45,45);
axis equal;
xlim([0,1000]);ylim([0,100]);zlim([0,100]);

% Create a logical image volume of a sphere with specified
% diameter, center, and image size.
% First create the image.
imageSizeX = 1000;
imageSizeY = 100;
imageSizeZ = 100;
[columnsInImage, rowsInImage, pagesInImage] = meshgrid(1:imageSizeX, 1:imageSizeY,1:imageSizeZ);

% Next create the fiber in the image.
fiberX = 0;
fiberY = 60;
fiberZ = 20;
fibRadius = 3;
fiberVoxels = (rowsInImage - fiberY).^2 + (pagesInImage - fiberZ).^2 ...
    <= fibRadius.^2;

fv = isosurface(fiberVoxels,0);
patch(fv,'FaceColor',[0 0 .7],'EdgeColor',[0 0 1]);
view(45,45);
axis equal;
xlim([0,1000]);ylim([0,100]);zlim([0,100]);